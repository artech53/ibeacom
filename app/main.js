import Vue from 'nativescript-vue'
import App from './components/1.EnterCodePage.vue'

import store from './store'


import { localize } from "nativescript-localize";
import VueDevtools from 'nativescript-vue-devtools'

if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}
  
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')
// Prints Colored logs when --env.production is *NOT* set while building
Vue.config.debug = (TNS_ENV !== 'production')


Vue.filter("L", localize);

new Vue({
  store,
  render: h => h('frame', [h(App)])
}).$start()
